
import HomePage from '../pages/home.f7.html';
import VenuePage from '../pages/venue.f7.html';
import CommitteePage from '../pages/committee.f7.html';
import TopicsPage from '../pages/topics.f7.html';
import SchedulePage from '../pages/schedule.f7.html';
import MyschedulePage from '../pages/myschedule.f7.html';

import DynamicRoutePage from '../pages/dynamic-route.f7.html';
import Page from '../pages/page.f7.html';
import Cards from '../pages/cards.f7.html';
import List from '../pages/list.f7.html';
import Blog from '../pages/blog.f7.html';
import News from '../pages/news.f7.html';
import Accordion from '../pages/accordion.f7.html';
import RequestAndLoad from '../pages/request-and-load.f7.html';
import NotFoundPage from '../pages/404.f7.html';

const apiUri = 'https://api.hemostas.ru/';
// const apiUri = 'http://localhost:8081/index.php/';
const offlineMessage = 'Для загрузки страницы нужен интернет.';
const params = {
  'access-token': '6IYzANkXf_CdDgvlHUzuo_HQFyRPJ8Ki',
  // 'access-token': 'z-aL3fQN12cMZTPLuyMi0iGfiDtZhDKG',
};
let state = {
  home: {
    title: "Российский форум по тромбозу и гемостазу",
    content: "10-я всероссийская (юбилейная) конференция по клинической гемостазиологии и гемореологии",
    action: "8-10 октября 2020",
    footer: "Москва, Россия",
    icon: "static/rfth.svg",
    reserved: "",
    after: "",
  },
};
const getSate = () => state;
const setState = newState => {
  console.log(newState);
  state = newState;
  console.log(state);
}
const sayError = (xhr) => {
  if (xhr.xhr === undefined) {
    return "";
  }
  const uri = xhr.xhr.requestParameters.url;
  return offlineMessage +
          '<br><small>' +
          // uri.substring(0, uri.indexOf("?")) +
          '<br> status: ' + xhr.status +
          '<br> message: ' + xhr.message +
          '</small>';
}
let dates = [];
var routes = [
  {
    path: '/',
    async: function (routeTo, routeFrom, resolve, reject) {
      const router = this;
      this.app.preloader.show();
      let context = {}, news = {};
      let version = 0;
      const render = () => resolve(
        {
          component: HomePage,
        },
        {
          context: {
            home: context,
            news: news,
            version: version,
          },
        }
      )
      let newRequest = Object.assign({
        cat: 3,
        sort: "-ts",
        "per-page": 1,
      }, params);
      // newRequest.cat = 3; // news category
      // newRequest.sort = "-ts"; // latest: sort by ts DESC
      // newRequest["per-page"] = 1; // reduce to one item only here
      this.app.request.promise.json(apiUri + 'v1/cards', newRequest)
        .then(function (data, status) {
          news = data.data;
        }).catch(function () {
          console.error("Failed to load news ");
          news = null;
        });
      this.app.request.promise.json(apiUri + 'v1/commons/1', params)
        .then(function (data, status) {
          version = +data.data.value;
          console.log("[Router] Minimal app version: " + version.toString());
        }).catch(function () {
          console.error("Failed to load minimal version ");
          version = 0;
        });
      this.app.request.promise.json(apiUri + 'v1/cards/1', params)
        .then(function (data, status) {
          console.log("Success");
          context = data.data;
          router.app.preloader.hide();
          render();
        }).catch(function () {
          context = getSate().home;
          router.app.preloader.hide();
          render();
        });
    },
  },
  {
    path: '/schedule/',
    async: function (routeTo, routeFrom, resolve, reject) {
      const router = this;
      this.app.preloader.show();
      let context = {}, dates = [];
      const render = () => resolve(
        {
          component: SchedulePage,
        },
        {
          context: {
            items: context,
            dates: dates,
            saved: typeof router.app.form.getFormData("schedule") === "undefined" ? {data: []} : router.app.form.getFormData("schedule"),
          },
        }
      )
      let newRequest = Object.assign({
        sort: "startTime",
      }, params);
      this.app.request.promise.json(apiUri + 'v1/schedules', newRequest)
        .then(function (data) {
          data.data.reduce((a, c) => {
            if (dates.indexOf(c.ts) < 0) dates.push(c.ts);
          });
          dates.forEach(item => {
            context[item] = [];
          });
          data.data.map((c) => {
            if (context.hasOwnProperty(c.ts)) {
              context[c.ts].push(c);
            }
            else {
              console.error(`Item ${c.id} with ts ${c.ts}: not found this date on context Object`);
            }
          });
          console.debug(context);
          router.app.preloader.hide();
          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '/myschedule/',
    async: function (routeTo, routeFrom, resolve, reject) {
      const router = this;
      this.app.preloader.show();
      let myschedule = {};
      const render = () => resolve(
        {
          component: MyschedulePage,
        },
        {
          context: {
            myschedule: myschedule,
            mdates: dates,
          },
        }
      )
      let newRequest = Object.assign({sort: "startTime"}, params);
      this.app.request.promise.json(apiUri + 'v1/schedules', newRequest)
        .then(function (data) {
          let store = router.app.form.getFormData("schedule");
          console.log("Store from local: ", store);
          if (typeof store === 'undefined') {
            store = {data: []};
            console.log("Store was reinitialised: ", store);
          }
          const storeInt = [];
          store.data.forEach(function(item, i) {
            console.log(item, +item);
            storeInt.push(+item)
          });
          console.log("Int store: ", storeInt);
          let myitems = data.data.filter(function (item) {
            let current = 0;
            storeInt.forEach((c, i) => {
              if (c === item.id) current = item.id;
            });
            // console.log(current, item.id, item);
            return current > 0;
          });
          console.log("Filtered items: ", myitems);
          if (myitems.length > 0) {
            console.log("$myitems length > 0, filtration");
            myitems.forEach((item, i) => {
              if (dates.indexOf(item.ts) < 0) dates.push(item.ts);
            });
            // myitems.reduce((a, c) => {
            //   console.log(dates.indexOf(c.ts));
            //   if (dates.indexOf(c.ts) < 0) dates.push(c.ts);
            // });
            console.debug(myitems, dates);
            dates.forEach(item => {
              myschedule[item] = [];
            });
            data.data.map((c) => {
              if (myschedule.hasOwnProperty(c.ts)) {
                if (storeInt.indexOf(c.id) > -1) {
                  myschedule[c.ts].push(c);
                }
              }
              // else {
              //   console.error(`Item ${c.id} with ts ${c.ts}: not found this date on myschedule Object`);
              // }
            });
          }
          console.debug("Myschedule: ", myschedule);
          router.app.preloader.hide();
          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '/venue/',
    component: VenuePage,
  },
  {
    path: '/committee/',
    async: function (routeTo, routeFrom, resolve, reject) {
      const router = this;
      this.app.preloader.show();
      let context, lead;
      const render = () => resolve(
        {
          component: CommitteePage,
        },
        {
          context: {
            persons: context,
            lead: lead,
          },
        }
      )
      this.app.request.promise.json(apiUri + 'v1/committees', Object.assign({sort: 'name'}, params))
        .then(function (data) {
          context = data.data;
          lead = context.find(s => s.id === 1);
          if (lead === undefined) {
            lead = context[0];
          }
          router.app.preloader.hide();
          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '/page/:id/',
    async: function (routeTo, routeFrom, resolve, reject) {
      const router = this;
      const pageId = routeTo.params.id;
      this.app.preloader.show();
      let context;
      const render = () => resolve(
        {
          component: Page,
        },
        {
          context: {
            page: context,
          },
        }
      )
      this.app.request.promise.json(apiUri + 'v1/pages/' + pageId, params)
        .then(function (data) {
          context = data.data;
          router.app.preloader.hide();
          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '/cards/:id/',
    async: function (routeTo, routeFrom, resolve, reject) {
      const router = this;
      const pageId = routeTo.params.id;
      this.app.preloader.show();
      let context;
      const render = () => resolve(
        {
          component: Cards,
        },
        {
          context: {
            page: context,
          },
        }
      )
      let paramsLocal = Object.assign({cat: pageId}, params);
      this.app.request.promise.json(apiUri + 'v1/cards', paramsLocal)
        .then(function (data) {
          context = data.data;
          router.app.preloader.hide();

          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '/accordion/:id/',
    async: function (routeTo, routeFrom, resolve, reject) {
      const router = this;
      const pageId = routeTo.params.id;
      this.app.preloader.show();
      let context;
      const render = () => resolve(
        {
          component: Accordion,
        },
        {
          context: {
            page: context,
          },
        }
      )
      let paramsLocal = Object.assign({}, params);
      paramsLocal.cat = pageId;
      this.app.request.promise.json(apiUri + 'v1/cards', paramsLocal)
        .then(function (data) {
          context = data.data;
          router.app.preloader.hide();

          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '/list/:id/',
    async: function (routeTo, routeFrom, resolve, reject) {
      const router = this;
      const pageId = routeTo.params.id;
      this.app.preloader.show();
      let context;
      const render = () => resolve(
        {
          component: List,
        },
        {
          context: {
            page: context,
          },
        }
      )
      let paramsLocal = Object.assign({cat: pageId}, params);
      this.app.request.promise.json(apiUri + 'v1/cards', paramsLocal)
        .then(function (data) {
          context = data.data;
          router.app.preloader.hide();
          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '/category/:id/',
    async: function (routeTo, routeFrom, resolve, reject) {
      const router = this;
      const pageId = routeTo.params.id;
      this.app.preloader.show();
      let context;
      const render = () => resolve(
        {
          component: Blog,
        },
        {
          context: {
            page: context,
          },
        }
      )
      let paramsLocal = Object.assign({
        cat: pageId,
        sort: "-ts",
      }, params);
      this.app.request.promise.json(apiUri + 'v1/cards', paramsLocal)
        .then(function (data) {
          context = data.data;
          router.app.preloader.hide();
          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '/topics/',
    component: TopicsPage,
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '/category/news/:newsId',
    async: function (routeTo, routeFrom, resolve, reject) {
      var router = this;
      var app = router.app;
      app.preloader.show();
      var newsId = routeTo.params.newsId;
      let context;
      const render = () => resolve(
        {
          component: News,
        },
        {
          context: {
            page: context,
          },
        }
      )
      this.app.request.promise.json(apiUri + 'v1/cards/' + newsId, params)
        .then(function (data) {
          context = data.data;
          router.app.preloader.hide();
          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;
      // App instance
      var app = router.app;
      // Show Preloader
      app.preloader.show();
      // User ID from request
      var userId = routeTo.params.userId;
      // render page function
      let context;
      const render = () => resolve(
        {
          component: RequestAndLoad,
        },
        {
          context: {
            user: context,
          },
        }
      )
      // Ajax Request
      this.app.request.promise.json(apiUri + 'v1/committees/' + userId, params)
        .then(function (data) {
          context = data.data;
          router.app.preloader.hide();
          render();
        }).catch(function (error) {
          router.app.preloader.hide();
          router.app.dialog.alert(sayError(error));
          console.log(error);
        });
    },
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
